module Main where

type Context = [(String, Bind)]

data Bind = BindName | VarBind Type

addBind :: Context -> String -> Bind -> Context
addBind ctx x bind = (x, bind):ctx

getBind :: Context -> Int -> Bind
getBind ctx index | index > length ctx = error " Index is greater than context" | otherwise = snd $ ctx !! (length ctx - index - 1)

data Type = BoolType | ArrType Type Type
deriving (Show,Eq)

data Term = Var Int Int | Lambda String Type Term | App Term Term | TermTrue | TermFalse | TermIf Term Term Term

getType :: Term -> Type
getType term = typeof term []

typeof :: Term -> Context -> Type
typeof term ctx = case term of
TermTrue -> BoolType
TermFalse -> BoolType
TermIf term1 term2 term3 ->
if (typeof term1 ctx) == BoolType then
let term2Type = typeof term2 ctx in
if term2Type == typeof term3 ctx then term2Type
else error "Different types of terms"
else error "Term type is not a boolean"

Var index _ ->
let bind = getBind ctx index in
case bind of
BindName -> error "No binding names found"
VarBind typ -> typ

Lambda arg argType t ->
let ctx' = addBind ctx arg (VarBind(argType)) in
let bodyType = typeof t ctx' in
ArrType argType bodyType

App t1 t2 ->
let t1Type = (typeof t1 ctx) in
let t2Type = (typeof t2 ctx) in
case t1Type of
ArrType t11Type t12Type ->
if t2Type == t11Type
then t12Type
else error "Different types in App"

_ -> error "App type is not an array"