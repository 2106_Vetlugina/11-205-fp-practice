data Expression = V String | L String Expression | A Expression Expression
deriving Show

show'(V a)=a
show'(L a b)="(λ "++a++". "++show' b++")"
show'(A a b)='(':show' a++" "++show' b++")"

eval :: Context -> Expression -> Expression
eval d(V a)=maybe(V a)id(lookup a d)
eval d(L a b)=L a.e d$b
eval d(A a b)=f d(e d a)(e d b)

apply d(L x s)q=e((x,q):d)s
apply d parse q=C p q
d=tail

parse('(':'λ':s)=let(V c,t)=parse(d s);(b,u)=parse(d.d$t);in(L c b,d u)
parse('(':s)=let(a,t)=parse s;(b,u)=parse(d t)in(A a b,d u)
parse(c:s)|elem c" .)"=(V "",c:s)|1<2=let((V w),t)=parse s in(V(c:w),t)

run = show'.eval[].fst.parse
main = do l<-getLine;putStrLn$r l