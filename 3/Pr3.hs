
{- +++ Хвостовая рекурсия +++ -}

fib 0 = 1
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

fac 0 = 1
fac n = n * fac (n-1)

fac2 n = helper n 1
    where helper 0 acc = acc
          helper n acc = helper (n-1) (n*acc)

fib2 :: Int -> Integer
fib2 n = helper n 1 1
    where helper 0 a b = a
          helper 1 a b = b
          helper n a b = helper (n-1) b (a+b)

{- синоним типа -}
type MyFun = Int -> Integer -> Integer -> Integer

helper :: MyFun
helper 0 a b = a
helper n a b = helper (n-1) b (a+b)

{- типы с помощью newtype -}
newtype Type1 = Type1Constructor Int

f1 :: Type1
f1 = Type1Constructor 5

f2 :: Type1 -> Int
f2 (Type1Constructor x) = x+1

newtype Dollars = Dollars Int
newtype Rubles = Rubles Int

-- compilation fails!
-- myBankIsStupid :: Rubles -> Dollars
-- myBankIsStupid x = x

{- типы с помощью data -}

data DataType = Constructor1
              | Constructor2 Int
              | Constructor3 Int Double
              | Constructor4 Int

g1 :: Int -> DataType
g1 0 = Constructor1
g1 x | odd x = Constructor2 x
     | even x = Constructor3 (x`mod`3) (fromIntegral x/5)

g2 :: DataType -> Int
g2 Constructor1 = 0
g2 (Constructor2 0) = 1
g2 (Constructor2 x) = x-1
g2 (Constructor3 x y) = truncate $ fromIntegral x*y

newtype TypeA = TypeA Int
data TypeB = TypeB Int

data TypeC = TypeC TypeB

g3 :: TypeC
g3 = TypeC (TypeB 5)

getTypeB :: TypeC -> TypeB
getTypeB (TypeC b) = b

data TypeD = TypeD
    { typeB :: TypeB
    , amount :: Int }
           | TDFun
    { fun :: Int -> Int }

g5 :: Int -> TypeD
g5 0 = TDFun (+1)
g5 x = TypeD (TypeB x) (x+1)



